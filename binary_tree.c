#include<stdio.h>
#include<stdlib.h>
#include<time.h>

struct node
{
	double key;																		//key of the node
	struct node *left;																//pointers to leaves with the same structure
	struct node *right;
};

void insert(double value, struct node **leaf)
{
	if(*leaf == NULL)																//check if the node is empty
	{
		*leaf = (struct node*)malloc(sizeof(struct node)); 							//if it is, allocate the memory for it
		(*leaf)->key = value;														//set the key equal to generated random number
		(*leaf)->left = NULL;														//set leaves as NULL
		(*leaf)->right = NULL;
	}
	else if(value < (*leaf)->key) insert(value, &(*leaf)->left);					//check if the value to be inserted is greater or less than key

	else if(value >= (*leaf)->key) insert(value, &(*leaf)->right);

	return;
}

void print(struct node *leaf, int p)
{
    int i;
    for(i=0; i<p; i++)
    {
        printf("-- -- ");															//just for the look of it
    }
    printf("%lf -> ",(*leaf).key);
    if((*leaf).left == NULL && (*leaf).right == NULL)
    {
        printf(" L: NULL R: NULL \n");												//if both are NULL printf NULL
    }
    else if((*leaf).left == NULL && (*leaf).right != NULL)
    {
        printf(" L: NULL R: %lf\n",(*(*leaf).right).key);							//if one is NULL, call function on another branch
        print((*leaf).right, p+1);
    }
    else if((*leaf).left != NULL && (*leaf).right == NULL)
    {
        printf(" L: %lf R: NULL\n",(*(*leaf).left).key);							//same as above, just in reverse
        print((*leaf).left, p+1);
    }
    else
    {
        printf(" L: %lf R: %lf\n",(*(*leaf).left).key, (*(*leaf).right).key);		//printf both keys, and call function for both branches
        print((*leaf).left, p+1);
        print((*leaf).right, p+1);
    }
    return;
}

void destroy(struct node *leaf)
{
	if(leaf != NULL)																//check if the leaf is empty
    {
		destroy(leaf->left);
		destroy(leaf->right);
      	free(leaf);																	//delete allocated memory
  	}
	return;
}

int main()
{
	int i;
	double random;
	struct node *root = NULL;
	srand(time(NULL));
	for(i=0; i<100; i++)
	{
		random = (double)rand()/RAND_MAX;
		insert(random, &root);
	}
	print(root, 0);
	destroy(root);
	return 0;
}
